/**
 * Created by Rini Daniel on 1/27/2017.
 */
export declare class AppSettings {
    static NEW_INFRASTRUCTURE_URL: string;
    static EXISTING_INFRASTRUCTURE_URL: string;
    static MONITORING_DATA_URL: string;
    static MONITORING_ALERT_SUMMARY_URL: string;
    static MONITORING_EVENT_SUMMARY_URL: string;
    static MONITORING_APPLICATION_INFO_URL: string;
    static MONITORING_CONTAINER_SUMMARY_URL: string;
    static MONITORING_DISKUTILITIES_SUMMARY_URL: string;
    static NEW_SERVICE_URL: string;
    static POST_IP_URL: string;
    static GET_LOG_INSIGHT_URL: string;
    static POST_LOG_INSIGHT_URL: string;
    static DATA_SET_MOCK: string;
    static MONITORING_APPLICATIONFACTS_INFO_URL: string;
    static MONITORING_APPLICATIONSTATUS_INFO_URL: string;
    static MONITORING_APPLICATIONSETTINGS_INFO_URL: string;
    static MONITORING_SERVERINFO_SUMMARY_URL: string;
    static MONITORING_SERVERFACTS_INFO_URL: string;
    static MONITORING_PROCESSDETAILS_INFO_URL: string;
    static MONITORING_PROCESSLIST_INFO_URL: string;
    static DATA_TOPO_MOCK: string;
    static PIE_DATA_MOCK: string;
    static DATA_TOPO_TREE_MOCK: string;
    static OVERALL_DATA_URL: string;
    static SERVER_DATA_URL: string;
    static APPLICATION_DATA_URL: string;
    static CPU_LOAD_DATA_URL: string;
    static NET_TRAFFIC_DATA_URL: string;
    static RESPONSE_TIME_DATA_URL: string;
    static REQUEST_PER_VM_DATA_URL: string;
    static MEMORY_SIZE_DATA_URL: string;
    static REQUEST_TIME_DATA_URL: string;
}
