import { OnInit } from '@angular/core';
export declare class WizzardComponent implements OnInit {
    currentStep: number;
    individualWidth: number;
    steps: any[];
    ngOnInit(): void;
}
