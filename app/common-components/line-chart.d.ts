import { EventEmitter, ElementRef } from '@angular/core';
export declare class LineChart {
    static isColorSet: boolean;
    options: Object;
    chartInstance: any;
    currentClickPoint: string;
    element: any;
    title: string;
    seriesTitle: string;
    series: any[];
    height: number;
    width: number;
    onFilterChange: EventEmitter<any>;
    saveInstance(chartInstance: Object): void;
    constructor(myElement: ElementRef);
    constructOptions(): {
        chart: {
            type: string;
            backgroundColor: string;
            height: number;
            width: number;
            padding: number;
        };
        credits: {
            enabled: boolean;
        };
        colors: string[];
        tooltip: {
            enabled: boolean;
            crosshairs: boolean[];
            formatter: () => string;
        };
        title: {
            text: string;
        };
        legend: {
            enabled: boolean;
            borderWidth: number;
            layout: string;
            backgroundColor: string;
            align: string;
            verticalAlign: string;
            floating: boolean;
            x: number;
            y: number;
        };
        xAxis: {
            type: string;
            tickPixelInterval: number;
            labels: {
                rotation: number;
                formatter: () => string;
            };
        };
        yAxis: {
            title: {
                text: any;
            };
            plotLines: {
                value: number;
                width: number;
                color: string;
            }[];
        };
        plotOptions: {
            line: {
                marker: {
                    enabled: boolean;
                    symbol: string;
                    radius: number;
                    states: {
                        hover: {
                            enabled: boolean;
                        };
                    };
                };
            };
            series: {
                fillOpacity: number;
                animationLimit: number;
                animation: {
                    duration: number;
                };
                point: {
                    events: {
                        legendItemClick: () => boolean;
                    };
                };
                allowPointSelect: boolean;
                showInLegend: boolean;
            };
        };
        series: {
            name: any;
            data: any;
        }[];
    };
    ngOnChanges(...args: any[]): void;
    ngOnInit(): void;
}
