import { EventEmitter } from '@angular/core';
import { PieSeries } from '../model/pieData-model';
export declare class FilterPieChart {
    static isColorSet: boolean;
    options: Object;
    chartInstance: any;
    currentClickPoint: string;
    title: string;
    seriesTitle: string;
    series: PieSeries[];
    height: number;
    width: number;
    onFilterChange: EventEmitter<any>;
    saveInstance(chartInstance: Object): void;
    mouseClick(e: any): void;
    mouseOver(e: any): void;
    mouseOut(e: any): void;
    constructOptions(): {
        chart: {
            type: string;
            backgroundColor: string;
            height: number;
            width: number;
            animation: boolean;
            margin: number;
            padding: number;
        };
        credits: {
            enabled: boolean;
        };
        legend: {
            align: string;
            verticalAlign: string;
            layout: string;
            itemStyle: {
                color: string;
                fontWeight: string;
                fontSize: string;
            };
        };
        tooltip: {
            enabled: boolean;
            pointFormat: string;
        };
        title: {
            text: string;
        };
        plotOptions: {
            series: {
                point: {
                    events: {
                        click: (e: any) => void;
                        legendItemClick: () => boolean;
                    };
                };
                animation: boolean;
                dataLabels: {
                    enabled: boolean;
                    distance: number;
                    color: string;
                    format: string;
                };
                allowPointSelect: boolean;
                showInLegend: boolean;
            };
        };
        series: {
            name: string;
            colorByPoint: boolean;
            data: any[] & PieSeries[];
        }[];
    };
    ngOnChanges(...args: any[]): void;
    ngOnInit(): void;
}
