import { EventEmitter, ElementRef } from '@angular/core';
export declare class AreaChart {
    static isColorSet: boolean;
    options: Object;
    chartInstance: any;
    currentClickPoint: string;
    element: any;
    title: string;
    seriesTitle: string;
    series: any[];
    height: number;
    width: number;
    onFilterChange: EventEmitter<any>;
    isStacked: boolean;
    max: number;
    saveInstance(chartInstance: Object): void;
    constructor(myElement: ElementRef);
    constructOptions(): {
        chart: {
            type: string;
            backgroundColor: string;
            height: number;
            width: number;
            padding: number;
        };
        credits: {
            enabled: boolean;
        };
        colors: string[];
        tooltip: {
            enabled: boolean;
            formatter: () => string;
        };
        title: {
            text: string;
        };
        legend: {
            enabled: boolean;
            borderWidth: number;
            layout: string;
            backgroundColor: string;
            align: string;
            verticalAlign: string;
            floating: boolean;
            x: number;
            y: number;
        };
        xAxis: {
            title: {
                text: any;
            };
            type: string;
            tickPixelInterval: number;
            labels: {
                rotation: number;
                formatter: () => string;
            };
        };
        yAxis: {
            title: {
                text: any;
            };
            max: number;
        };
        plotOptions: {
            area: {
                stacking: string;
                marker: {
                    enabled: boolean;
                    symbol: string;
                    radius: number;
                    states: {
                        hover: {
                            enabled: boolean;
                        };
                    };
                };
            };
            series: {
                fillOpacity: number;
                allowPointSelect: boolean;
                showInLegend: boolean;
            };
        };
        series: {
            name: any;
            data: any;
        }[];
    };
    ngOnChanges(...args: any[]): void;
    ngOnInit(): void;
}
