import { PieSeries } from '../model/pieData-model';
export declare class PieChart {
    options: Object;
    chartInstance: any;
    title: string;
    seriesTitle: string;
    series: PieSeries[];
    height: number;
    width: number;
    saveInstance(chartInstance: Object): void;
    constructOptions(): {
        chart: {
            type: string;
            backgroundColor: string;
            height: number;
            width: number;
            animation: boolean;
        };
        legend: {
            align: string;
            verticalAlign: string;
            layout: string;
        };
        tooltip: {
            pointFormat: string;
        };
        title: {
            text: string;
        };
        plotOptions: {
            series: {
                animation: boolean;
                dataLabels: {
                    enabled: boolean;
                    distance: number;
                    color: string;
                    format: string;
                };
                point: {
                    events: {
                        legendItemClick: () => boolean;
                    };
                };
                allowPointSelect: boolean;
                showInLegend: boolean;
            };
            pie: {
                colors: any[];
            };
        };
        series: {
            name: string;
            colorByPoint: boolean;
            animation: boolean;
            data: any[] & PieSeries[];
        }[];
    };
    ngOnChanges(...args: any[]): void;
    ngOnInit(): void;
}
