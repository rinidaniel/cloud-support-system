import { EventEmitter } from '@angular/core';
import { PieSeries } from '../model/pieData-model';
export declare class SplineChart {
    static isColorSet: boolean;
    options: Object;
    chartInstance: any;
    currentClickPoint: string;
    title: string;
    seriesTitle: string;
    series: PieSeries[];
    height: number;
    width: number;
    onFilterChange: EventEmitter<any>;
    saveInstance(chartInstance: Object): void;
    mouseClick(e: any): void;
    mouseOver(e: any): void;
    mouseOut(e: any): void;
    constructOptions(): {
        chart: {
            type: string;
            backgroundColor: string;
            height: number;
            width: number;
            padding: number;
        };
        credits: {
            enabled: boolean;
        };
        tooltip: {
            enabled: boolean;
            backgroundColor: string;
            borderColor: string;
            borderRadius: number;
            borderWidth: number;
            crosshairs: boolean[];
            formatter: () => string;
        };
        title: {
            text: string;
        };
        subtitle: {
            text: string;
        };
        legend: {
            enabled: boolean;
            borderWidth: number;
        };
        xAxis: {
            title: {
                text: string;
            };
            type: string;
            tickPixelInterval: number;
            labels: {
                rotation: number;
                formatter: () => string;
            };
        };
        yAxis: {
            title: {
                text: string;
            };
            plotLines: {
                value: number;
                width: number;
                color: string;
            }[];
        };
        plotOptions: {
            spline: {
                marker: {
                    enabled: boolean;
                    symbol: string;
                    radius: number;
                    states: {
                        hover: {
                            enabled: boolean;
                        };
                    };
                };
            };
        };
        series: {
            name: string;
            data: any[] & PieSeries[];
        }[];
    };
    ngOnChanges(...args: any[]): void;
    ngOnInit(): void;
}
