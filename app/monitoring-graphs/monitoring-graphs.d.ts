import { ChangeDetectorRef, EventEmitter } from '@angular/core';
import { MonitoringGraph } from '../model/monitoringGraph-model';
import { MonitoringGraphService } from '../services/monitoring-service';
export declare class MonitoringGraphView {
    private monitoringGraphService;
    private ref;
    monitoringGraph: MonitoringGraph;
    selectedFilter: string;
    subscription: any;
    timer: any;
    edited: boolean;
    onIPChange: EventEmitter<any>;
    constructor(monitoringGraphService: MonitoringGraphService, ref: ChangeDetectorRef);
    applyFilter(property: string, value: string): void;
    clearThisFilter(): void;
    removeFilter(filterKey: string): void;
    ngOnInit(): void;
    refreshPage(): void;
    callService(): void;
    ngOnDestroy(): void;
}
