/**
 * Created by Rini Daniel on 1/6/2017.
 */
export declare class ExistingInfrastructure {
    cloud_id: string;
    cloud_type: string;
    provider: string;
    req_name: string;
    status: string;
    time_stamp: string;
    constructor();
}
