export declare class Infrastructure {
    name: string;
    provider: string;
    zone: string;
    region: string;
    role: string;
    cloud_auth: CloudAuthorization;
    cluster_info: ClusterInformation;
    constructor();
}
export declare class CloudAuthorization {
    auth_url: string;
    username: string;
    password: string;
    tenant_name: string;
    region: string;
    identity_version: string;
    constructor();
}
export declare class ClusterInformation {
    count: string;
    name: string;
    flavor_details: FlavorDetails;
    image_details: ImageDetails;
    network_details: NetworkDetails;
    constructor();
}
export declare class FlavorDetails {
    flavor: string;
    override: string;
    create: string;
    name: string;
    vcpus: string;
    ram: string;
    disk: string;
    constructor();
}
export declare class ImageDetails {
    image: string;
    key_name: string;
    key_create: string;
    constructor();
}
export declare class NetworkDetails {
    override: string;
    public_net: string;
    private_net_name: string;
    dns: string;
    create_floating: string;
    constructor();
}
