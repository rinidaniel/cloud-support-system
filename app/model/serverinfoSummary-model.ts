export class ServerinfoSummary {
    disk: string;
    domain: string;
    fqdn: string;
    hostname: string;
    mac_address: string;
    number_of_nics: string;
    os: string;
    os_family: string;
    private_info:string;
    public_ip: string;
    ram:string;
    status:string;
    vcpus: string;
}