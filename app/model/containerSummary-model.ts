/**
 * Created  on 5/23/2017.
 */
    export class ContainerSummary {
        containerid: string;
        name: string;
        image: string;
        port: string;
        status: string;
    }
