/**
 * Created by Rini Daniel on 1/19/2017.
 */
export declare class LicenseInfo {
    package_type: string;
    issuer: string;
    key: string;
    start_date: string;
    end_date: string;
}
export declare class VmInfo {
    project: string;
    role: string;
    vm_list: string[];
    constructor();
}
export declare class NewServiceModel {
    service: string;
    version: string;
    action: string;
    os: string;
    release: string;
    license_info: LicenseInfo;
    vm_info: VmInfo;
    constructor();
}
