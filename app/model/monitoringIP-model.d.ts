export declare class CpuUtilisation {
    cpu_idle_time: number;
    cpu_user_time: number;
    cpu_system_time: number;
    cpu_iowait_time: number;
    cpu_nicetime: number;
}
export declare class NetworkTraffic {
    network_id: string;
    incoming_network_traffic: number;
    outgoing_network_traffic: number;
}
export declare class Value {
    clock: number;
    ns: number;
    cpu_load: number;
    cpu_utilisation: CpuUtilisation;
    memory_usage: number;
    network_traffic: NetworkTraffic[];
}
export declare class Result {
    host_name: string;
    status: string;
    disk_space: number;
    disk_used: number;
    values: Value[];
    constructor();
}
export declare class MonitoringIP {
    result: Result;
    cpuLoad: any[];
    memoryUsage: any[];
    inputNetworkTraffic: any[];
    outputNetworkTraffic: any[];
    cpuUtilisationIdle: any[];
    cpuUtilisationUser: any[];
    cpuUtilisationSystem: any[];
    cpuUtilisationIOWait: any[];
    cpuUtilisationNiceTime: any[];
    constructor();
    setData(valuesToSet: Value[]): void;
    readonly startDate: Date;
    getCpuLoad(): void;
    getmemoryUsage(): void;
    getnetworkTrafficInput(): void;
    getnetworkTrafficOutput(): void;
    getcpuUtilisationIdle(): void;
    getcpuUtilisationUser(): void;
    getcpuUtilisationSystem(): void;
    getcpuUtilisationIOWait(): void;
    getcpuUtilisationNiceTime(): void;
}
