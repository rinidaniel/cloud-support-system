export declare class PieSeries {
    name: string;
    y: number;
    constructor(name: string, val: number);
}
