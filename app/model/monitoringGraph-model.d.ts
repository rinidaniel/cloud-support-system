export interface VmData {
    ip_address: string;
    hostname: string;
    status: string;
    provider: string;
    commission_date: Date;
    operating_system: string;
    image: string;
    usage: string;
}
export interface Project {
    name: string;
    vm_data: VmData[];
}
export declare class MonitoringGraph {
    datacenter: string;
    projects: Project[];
    usageDistribution: any;
    statusDistribution: any;
    providerDistribution: any;
    osDistribution: any;
    initialProjects: Project[];
    filters: any;
    filterIPDistribution: string[];
    readonly filterKeys: Array<string>;
    readonly showUsageGraph: boolean;
    constructor();
    setData(dataObj: any): void;
    callAllFunctions(): void;
    setFilter(property: string, value: string): void;
    removeFilter(property: string): void;
    clearFilter(): void;
    applyFilter(): void;
    setStatusDistribution(): void;
    setProviderDistribution(): void;
    setOsDistribution(): any;
    setUsageDistribution(): any;
    setDistribution(forProperty: string, initialValue?: any): any;
    getIPDistribution(): any;
}
