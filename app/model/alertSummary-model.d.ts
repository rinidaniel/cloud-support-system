/**
 * Created by Rini Daniel on 1/17/2017.
 */
export declare class AlertSummary {
    ip_address: string;
    message: string;
    severity: string;
    reportingtime: string;
    classname: string;
}
