/**
 * Created by Rini Daniel on 2/2/2017.
 */
import { ChangeDetectorRef } from '@angular/core';
import { MonitoringIP } from '../model/monitoringIP-model';
import { ActivatedRoute, Router } from '@angular/router';
import { MonitoringIPService } from '../services/monitoringIP-service';
export declare class IPMonitoringView {
    private router;
    private monitoringIPService;
    private ref;
    monitoringIp: MonitoringIP;
    host: any;
    subscription: any;
    timer: any;
    isWidget: boolean;
    constructor(router: Router, route: ActivatedRoute, monitoringIPService: MonitoringIPService, ref: ChangeDetectorRef);
    ngOnInit(): void;
    ngOnDestroy(): void;
}
