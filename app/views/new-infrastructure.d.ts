import { InfrastructureService } from '../services/infrastructure-service';
import { Infrastructure } from '../model/infrastructure-model';
import { NotificationsService } from 'angular2-notifications';
import { Router } from '@angular/router';
export declare class NewInfrastructureView {
    private infrastructureService;
    private notificationService;
    private router;
    infra: Infrastructure;
    options: {
        timeOut: number;
        lastOnBottom: boolean;
        clickToClose: boolean;
        maxLength: number;
        maxStack: number;
        showProgressBar: boolean;
        pauseOnHover: boolean;
        preventDuplicates: boolean;
        preventLastDuplicates: string;
        rtl: boolean;
        animate: string;
        position: string[];
    };
    constructor(infrastructureService: InfrastructureService, notificationService: NotificationsService, router: Router);
    ngOnInit(): void;
    nextClicked(e: any, id: string): boolean;
    onSubmit(): void;
}
