import { LogInsightGet } from '../model/logInsightGet-model';
import { ActivatedRoute, Router } from '@angular/router';
import { LogInsightGetService } from '../services/logInsightGet-service';
export declare class LogInsightView {
    private router;
    private logInsightGetService;
    getLogList: LogInsightGet[];
    host: any;
    error_details: any;
    timestamp: Date;
    isWidget: boolean;
    constructor(router: Router, route: ActivatedRoute, logInsightGetService: LogInsightGetService);
    ngOnInit(): void;
}
