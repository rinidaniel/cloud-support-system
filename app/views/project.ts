import { Component, ChangeDetectorRef } from '@angular/core';
import { DatasetService } from '../services/dataset-service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
    moduleId: module.id,
    templateUrl: 'project.html',
    styleUrls: ['project.css'],
    providers: [DatasetService]
})
export class ProjectComponent {
     getareaChart: any;
    memoryLineChartData: any;
    isWidget: boolean;
    currentTab: string = 'dashboard';

    
    constructor(private router: Router,
              private ref: ChangeDetectorRef, private datasetService: DatasetService) {
    this.isWidget = false;
    router.events.subscribe((val) => {
      
    });
  }

  onPageChange(newIndex: number) {
  }

  
  


    ngOnInit() {
        this.getAllData();
    }
    changeTab(selectedTab) {
        if (this.currentTab !== selectedTab) {
            this.currentTab = selectedTab;
            this.getAllData();
        }
    }

    getAllData() {
        this.datasetService.getServiceData();
        // this.datasetService.updatePieDataset();
        // this.datasetService.updateTopologyDataset();
    }


}
