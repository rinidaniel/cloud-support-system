import { ChangeDetectorRef } from '@angular/core';
import { AlertSummary } from '../model/alertSummary-model';
import { AlertSummaryService } from '../services/alertSummary-service';
import { Router } from '@angular/router';
export declare class MonitoringView {
    private router;
    private alertSummaryService;
    private ref;
    alertList: AlertSummary[];
    initialAlertList: AlertSummary[];
    alertIPs: string[];
    isWidget: boolean;
    pageIndex: number;
    totalPages: number;
    itemsPerPage: number;
    constructor(router: Router, alertSummaryService: AlertSummaryService, ref: ChangeDetectorRef);
    onPageChange(newIndex: number): void;
    callAlerts(): void;
    createPageChunk(data: any[]): any[];
    onIPChange(filteredIPs: any[]): void;
    ngOnInit(): void;
}
