import { NewService } from '../services/newService-service';
import { NewServiceModel } from '../model/newService-model';
import { NotificationsService } from 'angular2-notifications';
import { Router } from '@angular/router';
export declare class NewServiceView {
    private newServiceInstance;
    private notificationService;
    private router;
    newServiceList: NewServiceModel[];
    updatingIndex: number;
    showModal: boolean;
    options: {
        timeOut: number;
        lastOnBottom: boolean;
        clickToClose: boolean;
        maxLength: number;
        maxStack: number;
        showProgressBar: boolean;
        pauseOnHover: boolean;
        preventDuplicates: boolean;
        preventLastDuplicates: string;
        rtl: boolean;
        animate: string;
        position: string[];
    };
    constructor(newServiceInstance: NewService, notificationService: NotificationsService, router: Router);
    onSubmit(): void;
    addRow(): void;
    edit(index: number): void;
    delete(index: number): void;
    hideModal(): void;
}
