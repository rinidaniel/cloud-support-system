export declare const DATA1: {
    "_id": string;
    "timestamp": number;
    "cpu": string;
    "memory": string;
    "disk": number;
    "network": number;
}[];
