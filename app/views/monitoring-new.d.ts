import { Router } from '@angular/router';
export declare class MonitoringNewView {
    private router;
    isWidget: boolean;
    data1: any;
    usageChart: any;
    memoryChart1: any;
    memoryChart2: any;
    memoryChart3: any;
    memoryChart4: any;
    errorChart: any;
    errorChart1: any;
    statusChart: any;
    networkChart: any;
    maximize: string;
    DATA1: any;
    DATA2: any;
    DATA3: any[];
    timer: any;
    subscription: any;
    refreshInterval: number;
    constructor(router: Router);
    resizeChart(chart: any, size: any): void;
    onMaximizeClick(element: any): any;
    ngOnInit(): void;
    ngOnDestroy(): void;
    getData1(count?: number): any[];
    getData2(count?: number): any[];
    getData3(count?: number): any[];
}
