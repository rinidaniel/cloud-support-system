import { ExistingInfrastructure } from '../model/existingInfrastructure-model';
import { ExistingInfrastructureService } from '../services/existing-infrastructure-service';
export declare class ExistingInfrastructureView {
    private existingInfrastructureService;
    existingInfraList: ExistingInfrastructure[];
    showModal: boolean;
    viewDetailsFor: ExistingInfrastructure;
    constructor(existingInfrastructureService: ExistingInfrastructureService);
    ngOnInit(): void;
    viewDetails(cloudId: string): void;
    hideModal(): void;
}
