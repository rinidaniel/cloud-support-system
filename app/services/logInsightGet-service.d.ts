import { Http, Headers } from '@angular/http';
import { LogInsightGet } from '../model/logInsightGet-model';
import { Observable } from 'rxjs';
export declare class LogInsightGetService {
    http: Http;
    headers: Headers;
    constructor(http: Http);
    getLogInsight(): Observable<LogInsightGet[]>;
    private extractResponse(res);
    private handleError(error);
}
