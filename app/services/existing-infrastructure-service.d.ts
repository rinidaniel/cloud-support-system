import { Http, Headers } from '@angular/http';
import { ExistingInfrastructure } from '../model/existingInfrastructure-model';
import { Observable } from 'rxjs';
export declare class ExistingInfrastructureService {
    http: Http;
    headers: Headers;
    constructor(http: Http);
    getExistingInfra(): Observable<ExistingInfrastructure[]>;
    private extractResponse(res);
    private handleError(error);
}
