import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs';
import { Http, Headers, Response } from '@angular/http';
import * as _ from 'lodash';
import { AppSettings } from '../settings';

@Injectable()
export class TopologyTreeDataService {
    private dataStore: any;
    private _topologyTreeDataset = new BehaviorSubject<any>(null);// any => class/interface Dataset
    get topologyTreeDataset(): Observable<object[]> {
        return this._topologyTreeDataset.asObservable();
    }
    headers: Headers;

    constructor(public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.dataStore = {
            topologyTreeDataset: null
        };
    }

    public getServiceData(): void {
        this.http.get(AppSettings.DATA_TOPO_TREE_MOCK)
            .map(this.extractTopoTreeResponse)
            .catch(this.handleError)
            .subscribe((data) => {
                this.dataStore.topologyTreeDataset = (data && data[0]) || null;
                this._topologyTreeDataset.next(this.dataStore.topologyTreeDataset);
            });
    }
    private extractTopoTreeResponse(res: Response): any {
        let body = res.json();
        return body;
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    } 

}