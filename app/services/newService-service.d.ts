import { Http, Headers } from '@angular/http';
import { NewServiceModel } from '../model/newService-model';
import { Observable } from 'rxjs';
export declare class NewService {
    http: Http;
    headers: Headers;
    constructor(http: Http);
    postNewRequest(newservice: NewServiceModel[]): Observable<string>;
    private extractResponse(res);
    private handleError(error);
}
