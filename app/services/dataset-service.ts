import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { Http, Headers, Response } from '@angular/http';
import * as _ from 'lodash';

import { AppSettings } from '../settings';

// import { Dataset } from "./dataset";

@Injectable()
export class DatasetService {
    private dataStore: any;
    private _networkTraffic = new BehaviorSubject<any[]>(null);// any => class/interface Dataset
    private _responseTime = new BehaviorSubject<any[]>(null);// any => class/interface Dataset
    private _requestpervm = new BehaviorSubject<any[]>(null);// any => class/interface Dataset
    private _memorySize = new BehaviorSubject<any[]>(null);// any => class/interface Dataset
    private _requestTime = new BehaviorSubject<any[]>(null);// any => class/interface Dataset
    private _statusPieData = new BehaviorSubject<any>(null);// any => class/interface Dataset
    private _providerPieData = new BehaviorSubject<any>(null);// any => class/interface Dataset
    private _topologyDataset = new BehaviorSubject<any>(null);// any => class/interface Dataset
    private _topologyTreeDataset = new BehaviorSubject<any>(null);// any => class/interface Dataset

    get networkTrafficDataset(): Observable<object[]> {
        return this._networkTraffic.asObservable();
    }
    get responseTimeDataset(): Observable<object[]> {
        return this._responseTime.asObservable();
    }
    get requestPerVmDataset(): Observable<object[]> {
        return this._requestpervm.asObservable();
    }
    get memorySizeDataset(): Observable<object[]> {
        return this._memorySize.asObservable();
    }
    
    get requestTimeDataset(): Observable<object[]> {
        return this._requestTime.asObservable();
    }
    get statusPieData(): Observable<object[]> {
        return this._statusPieData.asObservable();
    }
    get providerPieData(): Observable<object[]> {
        return this._providerPieData.asObservable();
    }
    get topologyDataset(): Observable<object[]> {
        return this._topologyDataset.asObservable();
    }
    get topologyTreeDataset(): Observable<object[]> {
        return this._topologyTreeDataset.asObservable();
    }
    headers: Headers;

    constructor(public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.dataStore = {
            networkTraffic: null,
            responseTime: null,
            requestpervm: null,
            memorySize: null,
            requestTime: null,
            statusPieData: null,
            providerPieData: null,
            topologyDataset: null,
            topologyTreeDataset: null
        };
    }

    public getRandomInt(min: number, max: number): any {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    public getServiceData(): void {
        this.http.get(AppSettings.DATA_SET_MOCK)
            .map(this.extractResponse)
            .catch(this.handleError)
            .subscribe((data) => {
                this.dataStore.networkTrafficDataset = data.networkTraffic;
                this.dataStore.responseTimeDataset = data.responseTime;
                this.dataStore.requestPerVmDataset = data.requestpervm;
                this.dataStore.memorySizeDataset = data.memorySize;
                this.dataStore.requestTimeDataset = data.requestTime;
                this._networkTraffic.next(this.dataStore.networkTrafficDataset);
                this._responseTime.next(this.dataStore.responseTimeDataset);
                this._requestpervm.next(this.dataStore.requestPerVmDataset);
                this._memorySize.next(this.dataStore.memorySizeDataset);
                this._requestTime.next(this.dataStore.requestTimeDataset);
            });

        this.http.get(AppSettings.DATA_TOPO_MOCK)
            .map(this.extractTopoResponse)
            .catch(this.handleError)
            .subscribe((data) => {
                this.dataStore.topologyDataset = data.network || {};
                this._topologyDataset.next(this.dataStore.topologyDataset);
            });

        this.http.get(AppSettings.DATA_TOPO_TREE_MOCK)
            .map(this.extractTopoTreeResponse)
            .catch(this.handleError)
            .subscribe((data) => {
                this.dataStore.topologyTreeDataset = data || {};
                this._topologyTreeDataset.next(this.dataStore.topologyTreeDataset);
            });

        this.http.get(AppSettings.PIE_DATA_MOCK)
            .map(this.extractPieResponse)
            .catch(this.handleError)
            .subscribe((data) => {
                this.dataStore.statusPieData = data.statusPie || [];
                this.dataStore.providerPieData = data.providerPie || [];
                this._statusPieData.next(this.dataStore.statusPieData);
                this._providerPieData.next(this.dataStore.providerPieData);
            });
    }

    private extractResponse(res: Response): any {
        let body = res.json();
        const networkTraffic: any[] = [];
        const memorySize: any[] = [];
        const responseTime: any[] = [];
        const requestpervm: any[] = [];
        const requestTime: any[] = [];
        _.each(body, (item: any) => {
            item.ip = item.ip || '0.0.0.0';
            item.port = item.port || 'NA';
            _.each(item.history, (hist:any) => {
                hist.value = +hist.value;
            })
            if (item.key === 'net.if.in' || item.key === 'net.if.out') {
                networkTraffic.push(item);
            }
            // if (item.key === 'vm.response.time' || item.key === 'vm.memory.requestpervm' || item.key === 'vm.memory.size[available]') {
            //     responseTime.push(item);
            // }
            if (item.key === 'vm.response.time') {
                responseTime.push(item);
            }
            // if (item.key === 'vm.memory.requestpervm' || item.key === 'vm.memory.size[total]') {
            //     requestpervm.push(item);
            // }
            if (item.key === 'vm.memory.requestpervm') {
                requestpervm.push(item);
            }
            if (item.key === 'vm.memory.size[available]' || item.key === 'vm.memory.size[total]') {
                memorySize.push(item);
            }
            if (item.key === 'system.request.time[avg1]' ||
                item.key === 'system.request.time[avg5]' || item.key === 'system.request.time[avg15]') {
                requestTime.push(item);
            }
        });
        return { networkTraffic, memorySize, responseTime, requestpervm, requestTime };
    }

    private extractTopoResponse(res: Response): any {
        let body = res.json();
        return body;
    }

    private extractTopoTreeResponse(res: Response): any {
        let body = res.json();
        return body;
    }

    private extractPieResponse(res: Response): any {
        let body = res.json();
        const statusPie = [];
        _.forIn(body.summary.status, function (value, key) {
            statusPie.push({
                label: key,
                value: +value,
                tooltip: key
            });
        });

        const providerPie = [];
        _.forIn(body.summary.provider, function (value, key) {
            providerPie.push({
                label: key,
                value: +value,
                tooltip: key
            });
        });

        return { statusPie, providerPie };
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    } 

}