import { Http, Headers } from '@angular/http';
import { Infrastructure } from '../model/infrastructure-model';
import { Observable } from 'rxjs';
export declare class InfrastructureService {
    http: Http;
    headers: Headers;
    constructor(http: Http);
    postNewInfra(infra: Infrastructure): Observable<string>;
    private extractResponse(res);
    private handleError(error);
}
