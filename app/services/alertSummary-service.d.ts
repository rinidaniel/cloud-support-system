import { Http, Headers } from '@angular/http';
import { AlertSummary } from '../model/alertSummary-model';
import { Observable } from 'rxjs';
export declare class AlertSummaryService {
    http: Http;
    headers: Headers;
    constructor(http: Http);
    getOverallAlertSummary(): Observable<AlertSummary[]>;
    getServerAlertSummary(): Observable<AlertSummary[]>;
    getApplicationAlertSummary(): Observable<AlertSummary[]>;
    private extractResponse(res);
    private handleError(error);
}
