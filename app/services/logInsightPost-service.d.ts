import { Http, Headers } from '@angular/http';
import { LogInsightPost } from '../model/logInsightPost-model';
import { Observable } from 'rxjs';
export declare class LogInsightPostService {
    http: Http;
    headers: Headers;
    constructor(http: Http);
    postLogInsight(logPost: LogInsightPost): Observable<string>;
    private extractResponse(res);
    private handleError(error);
}
