import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs';
import { Http, Headers, Response } from '@angular/http';
import * as _ from 'lodash';

import { AppSettings } from '../settings';

// import { Dataset } from "./dataset";

@Injectable()
export class NetworkTrafficService {
    private dataStore: any;
    private _networkTraffic = new BehaviorSubject<any[]>(null); // any => class/interface Dataset
    get networkTrafficDataset(): Observable<object[]> {
        return this._networkTraffic.asObservable();
    }
    headers: Headers;

    constructor(public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.dataStore = {
            networkTraffic: null,
        };
    }
    public getServiceData(): void {
        this.http.get(AppSettings.NET_TRAFFIC_DATA_URL)
            .map(this.extractResponse)
            .catch(this.handleError)
            .subscribe((data) => {
                this.dataStore.networkTrafficDataset = data.networkTraffic;
                this._networkTraffic.next(this.dataStore.networkTrafficDataset);
            });
    }

    private extractResponse(res: Response): any {
        let body = res.json();
        const networkTraffic: any[] = [];
        _.each(body, (item: any) => {
            item.ip = item.ip || '0.0.0.0';
            item.port = item.port || 'NA';
            _.each(item.history, (hist) => {
                hist.value = +hist.value;
            })
            if (item.key === 'net.if.in[eth1]' || item.key === 'net.if.in[eth0]') {
                networkTraffic.push(item);
            }
        });
        return { networkTraffic};
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

}