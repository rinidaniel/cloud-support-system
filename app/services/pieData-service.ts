import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs';
import { Http, Headers, Response } from '@angular/http';
import * as _ from 'lodash';

import { AppSettings } from '../settings';

// import { Dataset } from "./dataset";

@Injectable()
export class PieDataService {
    private dataStore: any;
    private _statusPieData = new BehaviorSubject<any>(null);// any => class/interface Dataset
    private _providerPieData = new BehaviorSubject<any>(null);// any => class/interface Dataset
    get statusPieData(): Observable<object[]> {
        return this._statusPieData.asObservable();
    }
    get providerPieData(): Observable<object[]> {
        return this._providerPieData.asObservable();
    }
    headers: Headers;

    constructor(public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.dataStore = {
            statusPieData: null,
            providerPieData: null
        };
    }

    public getServiceData(): void {

        this.http.get(AppSettings.PIE_DATA_MOCK)
            .map(this.extractPieResponse)
            .catch(this.handleError)
            .subscribe((data) => {
                this.dataStore.statusPieData = data.statusPie || null;
                this.dataStore.providerPieData = data.providerPie || null;
                this._statusPieData.next(this.dataStore.statusPieData);
                this._providerPieData.next(this.dataStore.providerPieData);
            });
    }

    private extractPieResponse(res: Response): any {
        let body = res.json();
        const statusPie = [];
        _.forIn(body.summary.status, function (value, key) {
            statusPie.push({
                label: key,
                value: +value,
                tooltip: key
            });
        });

        const providerPie = [];
        _.forIn(body.summary.provider, function (value, key) {
            providerPie.push({
                label: key,
                value: +value,
                tooltip: key
            });
        });

        return { statusPie, providerPie };
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    } 

}