import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs';
import { Http, Headers, Response } from '@angular/http';
import * as _ from 'lodash';
import { AppSettings } from '../settings';


@Injectable()
export class CPULoadService {
    private dataStore: any;
    private _cpuLoad = new BehaviorSubject<any[]>(null);// any => class/interface Dataset
    get cpuLoadDataset(): Observable<object[]> {
        return this._cpuLoad.asObservable();
    }
    headers: Headers;

    constructor(public http: Http) {
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.dataStore = {
            cpuLoadDataset: null
        };
    }

    public getServiceData(): void {
        this.http.get(AppSettings.CPU_LOAD_DATA_URL)
            .map(this.extractResponse)
            .catch(this.handleError)
            .subscribe((data) => {
                this.dataStore.cpuLoadDataset = data.cpuLoad;
                this._cpuLoad.next(this.dataStore.cpuLoadDataset);
            });
    }

    private extractResponse(res: Response): any {
        let body = res.json();
        const cpuLoad: any[] = [];
        _.each(body, (item: any) => {
            item.ip = item.ip || '0.0.0.0';
            item.port = item.port || 'NA';
            _.each(item.history, (hist:any) => {
                hist.value = +hist.value;
            })
            if (item.key === 'system.cpu.load[percpu,avg1]' ||
                item.key === 'system.cpu.load[percpu,avg5]' || item.key === 'system.cpu.load[percpu,avg15]') {
                cpuLoad.push(item);
            }
        });
        return { cpuLoad };
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);

}
}