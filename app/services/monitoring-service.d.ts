import { Http, Headers } from '@angular/http';
import { MonitoringGraph } from '../model/monitoringGraph-model';
import { Observable } from 'rxjs';
export declare class MonitoringGraphService {
    http: Http;
    headers: Headers;
    constructor(http: Http);
    getMonitoringData(): Observable<MonitoringGraph>;
    private extractResponse(res);
    private handleError(error);
}
