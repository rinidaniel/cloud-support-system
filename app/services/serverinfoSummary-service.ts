import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {ServerinfoSummary} from '../model/serverinfoSummary-model';
import {AppSettings} from '../settings';
import {Observable} from 'rxjs';

@Injectable()
export class ServerinfoSummaryService {
  headers: Headers;

  constructor(public http: Http) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/json');
  }

  getServerinfoSummary (): Observable<ServerinfoSummary[]> {
    return this.http.get(AppSettings.MONITORING_SERVERINFO_SUMMARY_URL)
      .map(this.extractResponse)
      .catch(this.handleError);
  }
 

  private extractResponse(res: Response): ServerinfoSummary[] {
    let body = res.json();
    return body.map(function (serverinfo: any) {
      const transformedserverinfo = new ServerinfoSummary();
      transformedserverinfo.disk = serverinfo.disk;
      transformedserverinfo.domain = serverinfo.domain;
      transformedserverinfo.fqdn = serverinfo.fqdn;
      transformedserverinfo.hostname = serverinfo.hostname;
      transformedserverinfo.mac_address = serverinfo.mac_address;
      transformedserverinfo.number_of_nics = serverinfo.number_of_nics;
      transformedserverinfo.os = serverinfo.os;
      transformedserverinfo.os_family = serverinfo.os_family;
      transformedserverinfo.private_info = serverinfo.private_info;
      transformedserverinfo.public_ip = serverinfo.public_ip;
      transformedserverinfo.ram = serverinfo.ram;
      transformedserverinfo.status = serverinfo.status;
      transformedserverinfo.vcpus = serverinfo.vcpus;
      console.log(transformedserverinfo);
      console.log('This is working');
      return transformedserverinfo;    
    });
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}