import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { MonitoringIP } from '../model/monitoringIP-model';
export declare class MonitoringIPService {
    http: Http;
    headers: Headers;
    constructor(http: Http);
    getIPDetails(host: string): Observable<MonitoringIP>;
    private extractResponse(res);
    private handleError(error);
}
