import { Title } from '@angular/platform-browser';
export declare class AppService {
    private titleService;
    baseTitle: string;
    titleSeparator: string;
    constructor(titleService: Title);
    inferTitleFromUrl(url: string): void;
    setTitle(title: string): void;
    getTitle(): string;
}
