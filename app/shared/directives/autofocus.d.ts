import { ElementRef, Renderer } from '@angular/core';
export declare class Autofocus {
    private renderer;
    private el;
    constructor(renderer: Renderer, el: ElementRef);
    ngOnInit(): void;
}
